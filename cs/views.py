from django import http
from django.http import HttpResponse
from django.shortcuts import redirect, render
import requests, json

fetch_details_api = "https://live.eurekaacademy.co.in/?route=common/ajax&mod=liveclasses&ack=getVideoDatabyClassId&classId="
get_cookie_api = "https://www.toprankers.com/?route=common/ajax&mod=liveclasses&ack=getcustompolicysignedcookiecdn&fromWeb=1&stream="
    
def index(request):
    return render(request, "return_link.html")

def fetchs(id):
    r = requests.get(fetch_details_api + str(id))
    # print(r.text)
    return r.text

def fetch(request):
    if not request.POST:
        return redirect(index)
        # return HttpResponse("Invalid access.")
    else:
        if "?" in request.POST["link"]:
            id = request.POST["link"].strip().split("?")[0].split("-")[-1] 
        else:
            id = request.POST["link"].strip().split("-")[-1]

        datas = json.loads(fetchs(id))
        print(datas)

        m3u8_file = datas["data"]["primPlaybackUrl"]
        if m3u8_file != None:
            coo = get_cookie_api + datas["data"]["primPlaybackUrl"]
        else:
            coo = "Video upload pending."
        
        context = { "title" : datas["data"]["primaryStream"],
        "video" : datas["data"]["primPlaybackUrl"], 
        "cookie_url" : coo
         }


        return render(request, "video.html", context)

def api(request, url):
    context = {
        "url" : str(url)
    }

    if url:
        try:
                
            if "?" in url:
                id = url.strip().split("?")[0].split("-")[-1] 
            else:
                id = url.strip().split("-")[-1]

            datas = json.loads(fetchs(id))
            print(datas)

            m3u8_file = datas["data"]["primPlaybackUrl"]
            if m3u8_file != None:
                coo = get_cookie_api + datas["data"]["primPlaybackUrl"]
            else:
                coo = "Video upload pending."
            
            context = { "title" : datas["data"]["primaryStream"],
            "video" : datas["data"]["primPlaybackUrl"], 
            "cookie_url" : coo
            }
            return render(request, "video.html", context)
        except:
            return HttpResponse("""
            Error in parsing. \n Try from <a href="https://live.eurekaacademy.co.in/tgt-computer-science"> Click Here</a>
            """)
        
    else:
        url = "missing"
        return HttpResponse("API is live. URL is " + url)