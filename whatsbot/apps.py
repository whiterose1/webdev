from django.apps import AppConfig


class WhatsbotConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'whatsbot'
